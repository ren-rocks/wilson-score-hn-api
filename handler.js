'use strict';

const axios = require('axios');
const {
    DynamoDBClient,
    GetItemCommand,
    PutItemCommand
} = require('@aws-sdk/client-dynamodb');
const {
    S3Client,
    PutObjectCommand
} = require("@aws-sdk/client-s3");

const ENDPOINTS = Object.freeze({
    POSTS: 'https://hacker-news.firebaseio.com/v0/topstories.json',
    POST: 'https://hacker-news.firebaseio.com/v0/item/{id}.json'
});

const AWS_S3_CONFIG = Object.freeze({
    IndexKey: 'index.json',
    Bucket: process.env.BUCKET_NAME
})

const AWS_CLIENT_CONFIG = Object.freeze({
    region: "us-east-1",
});

const AWS_DDB_CONFIG = Object.freeze({
    TableName: process.env.DDB_TABLE_NAME,
    idsKey: 'ids',
})

const getFromDDB = async (key, client) => {
    const { TableName } = AWS_DDB_CONFIG;
    const index = { S: key }

    const command = new GetItemCommand({
        TableName,
        Key: { index }
    });

    const { Item } = await client.send(command);
    const { data } = Item;
    const { S: items } = data;
    return JSON.parse(items || "{}");;
};

const updateToDDB = async (key, content, client) => {
    const { TableName } = AWS_DDB_CONFIG;
    const index = { S: key }
    const data = { S: content }
    const Item = {
        index,
        data
    };

    const command = new PutItemCommand({
        TableName,
        Item
    });
    
    await client.send(command);
}

const updateToS3 = async (Key, Arr, client) => {
    const { Bucket } = AWS_S3_CONFIG

    const command = new PutObjectCommand({
        Bucket,
        Body: JSON.stringify(Arr),
        Key,
        ContentType: "application/json"
    });

    await client.send(command)
}

module.exports.scrape = async event => {
    let statusCode = 200;
    let success = true;
    let error = {};

    const client = new DynamoDBClient(AWS_CLIENT_CONFIG);

    try {
        const { data: ids} = await axios.get(ENDPOINTS.POSTS);
        await updateToDDB(AWS_DDB_CONFIG.idsKey, JSON.stringify(ids.slice(0, 200)), client);
    }
    catch(err) {
        statusCode = 500;
        success = false;
        error = err;
    }

    return {
        statusCode,
        body: JSON.stringify({ 
            success,
            error,
            event
        }, null, 2 ),
    };
};

module.exports.update = async event => {
    let statusCode = 200;
    let success = true;
    let error = {}

    const ddbClient = new DynamoDBClient(AWS_CLIENT_CONFIG);

    let newPosts = [];
    try {
        const ids = await getFromDDB(AWS_DDB_CONFIG.idsKey, ddbClient);
        for(let i = 0; i < ids.length; i++) {
            const id = ids[i];
            const { data } = await axios.get(ENDPOINTS.POST.replace("{id}", id));
            const { 
                title,
                url,
                score,
                descendants,
                time: date
            } = data;

            newPosts.push({
                title,
                url,
                score: (score - descendants),
                date
            });
        }
    }
    catch(err) {
        statusCode = 500;
        success = false;
        error = err;
    }

    newPosts = newPosts.sort((a, b) => b.score - a.score);
    
    if (newPosts.length > 50) {
        const s3Client = new S3Client(AWS_CLIENT_CONFIG);
        await updateToS3(AWS_S3_CONFIG.IndexKey, newPosts, s3Client).catch(err => {
            statusCode = 500;
            success = false;
            error = err;
        });
    }

    return {
        statusCode,
        body: JSON.stringify({ 
            success,
            error,
            event
        }, null, 2 ),
    };
};
